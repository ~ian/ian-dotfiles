; Ian Jackson's Emacs startup file - runs ~ian/emacs/ian.el
;
(setq load-path (append '("~@@$username@@/emacs") load-path))
(load-library "ian")
(menu-bar-mode -1)
