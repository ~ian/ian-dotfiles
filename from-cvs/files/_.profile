# Ian Jackson's .profile now in CVS

. .configs/setenvir

echo --------------------------------------------------
uptime

if [ "x$TERM" = "xnetwork" ]
then
	defterm=vt100
	echo -n "Terminal type (default=$defterm) ... "
	read TERM
	[ "x$TERM" = x ] && TERM=$defterm
	export TERM
fi

[ "x`tty`" = x/dev/console ] || stty cs8 2>/dev/null || true

exec $SHELL
