; Fixup of compile so it doesn't ask unless you want it to.

(require 'compile)
(provide 'quicker-compile)

(defun quicker-compile (prompt-for-command)
  "Runs compile. Will prompt for the compile command only if given
a prefix argument."
  (interactive "P")
  (if prompt-for-command
      (setq compile-command (read-string "Compile command: " compile-command)))
  (compile compile-command))
