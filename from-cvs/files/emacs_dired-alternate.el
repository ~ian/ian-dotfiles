; dired-alternate.el
; (C)1991 Ian Jackson
; This file is placed under the protection of the GNU General Public Licence.
;
; Patch to make "F" in dired-mode replace the current (dired) buffer with
; a buffer visiting the file the cursor is on.
;
; Also new dired-run-file function.

(defun dired-find-alternate-file ()
  "Like dired-find-file and find-alternate-file put together"
  (interactive)
  (find-alternate-file (dired-get-filename)))

(defun dired-run-file ()
  "Runs the file pointed to in dired - output if any is put in the *Shell Command Output* buffer"
  (interactive)
  (let ((file (dired-get-filename))
	(shelco (get-buffer-create "*Shell Command Output*"))
	(oldbuf (current-buffer)))
    (message "Running %s..." file)
    (set-buffer shelco)
    (erase-buffer)
    (set-buffer-modified-p nil)
    (call-process file nil shelco nil)
    (if (buffer-modified-p)
	(display-buffer shelco))
    (message "Running %s...done" file)
    (set-buffer oldbuf)
    (revert-buffer)))
