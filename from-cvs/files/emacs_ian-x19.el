; Ian's X, Emacs-19 (and higher) initialisations

;(setq frame-title-format
;      '(multiple-frames "%b" ("" invocation-name " : "
;                              user-real-login-name "@" system-name)))

;(setq default-frame-alist
;      (append '((internal-border-width . 1)) default-frame-alist))
;(scroll-bar-mode nil)
;(modify-frame-parameters (selected-frame) '((internal-border-width . 1)))
;(let ((bd (cdr (assoc 'border-width (frame-parameters)))))
;  (set-frame-position (selected-frame)
;                      (- 1024 ; was (string-to-number (ians-configure "pixels_x"))
;                         (+ (frame-pixel-width) bd))
;                      bd))

(setq mouse-yank-at-point t)
(setq x-pointer-shape x-pointer-left-ptr)
(set-mouse-color (cdr (assoc 'mouse-color (frame-parameters))))

(global-set-key [mode-line mouse-1]
                '(lambda (event) (interactive "e\n")
                   (let ((old-window (selected-window)))
                     (unwind-protect
                         (progn (select-window (posn-window (event-end event)))
                                (scroll-up))
                       (select-window old-window)))))

(global-set-key [mode-line C-mouse-3] 'mouse-delete-window)

(global-set-key [mode-line mouse-3]
                '(lambda (event) (interactive "e\n")
                   (let ((old-window (selected-window)))
                     (unwind-protect
                         (progn (select-window (posn-window (event-end event)))
                                (scroll-down))
                       (select-window old-window)))))

(global-set-key [mode-line down-mouse-2] 'mouse-buffer-menu)

(global-unset-key [C-down-mouse-1])
(global-set-key [C-mouse-1] 'save-buffer)
(global-unset-key [C-down-mouse-2])
(global-set-key [C-mouse-2] 'mouse-kill)

(fset 'run-programs-menu '(keymap
                           "Programs"
                           ([compile] . ("Quicker Compile" . quicker-compile))
                           ([vm] . ("VM" . vm))))

(global-unset-key [down-C-mouse-3])
(global-set-key [down-C-mouse-3] 'run-programs-menu)

(global-set-key [mouse-2] 'mouse-yank-at-click)
(global-set-key [mouse-3] 'mouse-save-then-kill)

(setq interprogram-paste-function
      '(lambda () (x-get-cut-buffer 0)))

;      (append '((vertical-scroll-bars . nil) (horizontal-scroll-bars . nil)
;		(menu-bar-lines . 0))
;	      default-frame-alist))

;(if (assoc 'user-position default-frame-alist) t
;  (setq default-frame-alist
;        (append '((top . 0) (left . -1)) 
;		default-frame-alist))
;  (set-frame-position (selected-frame) -1 0))

;(if (equal (ians-configure "colours") "mono")
;    (progn
;      (setq default-frame-alist
;            (append '((foreground-color . "black") (background-color . "white")
;                      (mouse-color . "white") (cursor-color . "black"))
;                    default-frame-alist))
;      (set-face-foreground 'modeline "white")
;      (set-face-background 'modeline "black")
;      (modify-frame-parameters (selected-frame) '((cursor-color . "black")))
;      ))
;(if (equal (ians-configure "colours") "mono")
;    (setq inverse-video t))
;
; (append '((foreground-color . "black") (background-color . "white")
;	 	    (mouse-color . "white") (cursor-color . "black"))
;                    default-frame-alist)))

(provide 'ian-x19)
