;;; x-fix-mouse.el
;;;
;;; make mouse-based paste ignore the mouse position and use the emacs point
;;; instead.  Make cut-text set the mark so the mouse can be used naturally
;;; to select a region (e.g. to indent code).
;;;
;;; Jean-Francois Lamy 1989-09-21
;;; lamy@ai.utoronto.ca

(require 'x-mouse); These two lines
(provide 'x-fix-mouse); added by 89iwj@eng.cam.ac.uk

(defun x-cut-text (arg &optional kill)
  "Copy text between point and mouse into window system cut buffer.
Set mark to current mouse position. Save in Emacs kill ring also."
  (if (coordinates-in-window-p arg (selected-window))
      (progn
       (x-mouse-set-mark arg)
	 (let ((beg (point)) (end (mark)))
	   (x-store-cut-buffer (buffer-substring beg end))
	   (copy-region-as-kill beg end)
	   (if kill (delete-region beg end))))
    (message "Mouse not in selected window")))

(defun x-paste-text (arg)
  "Insert window system cut buffer contents at current point."
  (insert (x-get-cut-buffer)))

(defun x-cut-and-wipe-text (arg)
  "Kill text between point and mark; also copy to window system cut buffer."
  (x-cut-text arg t))

(defun x-cut-text-if-moved (arg &optional kill)
  (let ((opoint (point)))
    (x-mouse-set-point arg)
    (cond 
     ((not (equal (point) opoint))
      (goto-char opoint)
      (x-cut-text arg kill)))))

;------- End of Forwarded Message
