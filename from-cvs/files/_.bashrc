# Ian Jackson's shell rc

hostname_completion_file=~/.hosts 

export PWD

# My own functions
ls() { /bin/ls -FC "$@"; }
ll() { /bin/ls -FCla "$@"; }
setprompt() { PS1=`autotitle`; export PS1; }
rs() { eval `/usr/openwin/bin/resize` ; setprompt ; }
cd() { builtin cd "$@"; setprompt; }
pu() { builtin pushd "$@"; setprompt; }
po() { builtin popd "$@"; setprompt; }

# Set up history and prompt
if [ "$PS1" ]
then
	HISTFILE=$HOME/.history/$HOST~$$~
	AUTOTITLE_STDOUT="@@?
 $chiark && $username eq 'ian' ?@@-@@;%?u%U%!%?g(%G)%!%?u@%!%h:%d1~.> "
 	AUTOTITLE_WINDOW="%h - %d~. %Y.[U..[M..[D./..%X.L]..M]..R]./.. %U%?G(%G)%!"
	AUTOTITLE_ICON="%h: %d3~. %Y.[U..[M..[D./..%X.L]..M]..R]./.."
	export AUTOTITLE_STDOUT AUTOTITLE_WINDOW AUTOTITLE_ICON
	setprompt
fi
