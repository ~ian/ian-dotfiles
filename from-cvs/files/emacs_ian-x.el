; Ian's X initialisations.

(if (not (string-match "^18\." emacs-version))
    (require 'ian-x19)
    
  (require 'x-fix-mouse)
  (define-key mouse-map x-button-left-up 'x-cut-text-if-moved)
  (define-key mouse-map x-button-middle 'x-cut-text)
  (define-key mouse-map x-button-c-middle 'x-cut-and-wipe-text)
  (define-key mouse-map x-button-right 'x-paste-text))

; (x-set-cursor-color "white")
; (x-set-foreground-color "white")
; (x-set-background-color "black")

(if (boundp 'tool-bar-mode)
    (tool-bar-mode -1))
  
; End of this file.
