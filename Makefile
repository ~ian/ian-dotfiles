#

install:
	infra/makelinks setup
	$(MAKE) -C autotitle install

import:
	infra/makelinks import

check:
	infra/makelinks list

.PHONY: install
