;;    Ian Jackson's mail nicknames file.    -*- fundamental -*-
;;                                         (don't edit as elisp)

;; Address translations for PGP
(setq autopgp-outgoing-address-translation-alist '(
  ("ian".				  "Ian Jackson <ijackson@nyx.cs.du.edu>")
  ("Ian Jackson <ian@chiark>".		  "Ian Jackson <ijackson@nyx.cs.du.edu>")
  ("Owen Dunn <O.S.Dunn@cai.cam.ac.uk>".  "Owen S. Dunn <O.S.Dunn@cai.cam.ac.uk>")
  ("Stephen Early <sde1000@cam.ac.uk>".   "Stephen Early <sde1000@hermes.cam.ac.uk>")
  ("Pete.Chown@dale.dircon.co.uk".        "Pete Chown <pc@dale.demon.co.uk>")
  ("(Pete Chown) pc@dale.demon.co.uk".    "Pete Chown <pc@dale.demon.co.uk>")
  ("Richard Brooksby <richard@harlequin.co.uk>".
                                          "Richard Brooksby <richard@harlqn.co.uk>")
  ("Ron Pritchett <pritchet@scsn.net>".
                                "Ron Pritchett <pritchet@usceast.cs.scarolina.edu>")
  ("szahn%masterix@goesser.sie.siemens.co.at".
                                          "Steffen Zahn <szahn@ets5.uebemc.siemens.de>")
  ("Phil Karn <karn@unix.ka9q.ampr.org>". "Phil Karn <karn@qualcomm.com>")
  ("Alan Bain <afrb2@hermes.cam.ac.uk>".  "Alan F. R. Bain <afrb2@cam.ac.uk>")
  ("Andrew Burt <aburt@nyx.cs.du.edu>".
                                          "Andrew Burt <aburt@cs.du.edu>")
  ("Grant.Denkinson@nottingham.ac.uk".
                                "Grant W. Denkinson <G.W.Denkinson@geog.nott.ac.uk>")
  ("Ian Murdock <imurdock@gnu.ai.mit.edu>".
                                          "Ian Murdock <imurdock@debian.org>")
  ("Ian A. Murdock <imurdock@debian.org>"."Ian Murdock <imurdock@debian.org>")
  ("Tim Morley <tim@island.demon.co.uk>". "Tim Morley <tim@derwent.co.uk>")
  ("Eva R. Myers <erm1001@phy.cam.ac.uk>". "Eva Rebecca Myers <erm1001@cam.ac.uk>")
  ("Patrick.Weemeeuw@kulnet.kuleuven.ac.be".
                  "Patrick J.G.C. Weemeeuw <patrick.weemeeuw@kulnet.kuleuven.ac.be>")
  ("Allen Wheelwright <apw24@hermes.cam.ac.uk>".
                                          "Allen Wheelwright <apw24@cam.ac.uk>")
))
