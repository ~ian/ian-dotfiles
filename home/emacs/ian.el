; This is my real emacs start-up file (for chiark).
; 

(defalias 'perl-mode 'cperl-mode)
(setq cperl-invalid-face nil)
;      cperl-indent-level 4
;      cperl-continued-statement-offset 4)

(load-library "vm-reply")

(make-variable-buffer-local 'indent-line-function)
; Un-disable narrow and eval-expression
(put 'narrow-to-region 'disabled nil)
(put 'eval-expression 'disabled nil)
;(standard-display-european t)

(setq visible-cursor nil)

(setq browse-url-netscape-program "mozilla")
(setq common-lisp-hyperspec-root "/usr/share/doc/hyperspec/")
;
; Make sure we don't disturb links or change ownership, use numbers
(setq backup-by-copying-when-mismatch t)
(setq backup-by-copying-when-linked t)
;(setq version-control t
;      kept-new-versions 4
;      kept-old-versions 4
;      trim-versions-without-asking t)
;
; Various one-liners
(setq require-final-newline "ask")
(if (string= window-system 'x) (load-library "ian-x"))
(load-library "ian-aliases")
; (load-library "auto-pgp")
; (load-library "debian-changelog-mode")
; (if (string= (getenv "TERM") "bbcb32") (setq term-file-prefix nil))
; (setq explicit-shell-file-name "/usr/local/bin/tcsh")
(setq inhibit-local-variables t)
(setq enable-local-variables :safe)
(setq enable-recursive-minibuffers t)
(setq compile-command "make")
(setq next-line-add-newlines t)
(setq diff-switches "-u")
(setq line-move-visual nil)
(display-time)
;
(set-default 'indent-line-function 'insert-tab)
(defun defuse-electrocuting-indent ()
  "Disable electric-indent-mode if indent-line-function is daft"
  (and
   (eq indent-line-function 'insert-tab)
   (electric-indent-local-mode 0)))
(add-hook 'after-change-major-mode-hook 'defuse-electrocuting-indent)

(eval-after-load "sh-script"
  '(add-hook 'sh-mode-hook (lambda () (sh-electric-here-document-mode 0))
	     t))
;
(set-variable 'search-repeat-char 28)
(set-fringe-mode (cons 0 nil))
(if (string-match "^19\." emacs-version)
    (progn
;      (setq file-name-handler-alist     ; Disable nasty auto-loading of ange-ftp
;            (delq (rassq 'ange-ftp-completion-hook-function file-name-handler-alist)
;                  file-name-handler-alist))
      (require 'find-alternate-18style)
      (defun unset-down-mouse-23 ()
        (local-set-key [mouse-2] 'mouse-yank-at-click)
        (local-unset-key [down-mouse-2])
        (local-unset-key [down-mouse-3])
        (local-unset-key [C-down-mouse-3]))
      (add-hook 'vm-mode-hook 'unset-down-mouse-23)
      (add-hook 'vm-mail-mode-hook 'unset-down-mouse-23)
      (add-hook 'text-mode-hook 'unset-down-mouse-23)
      (add-hook 'Info-mode-hook
                (function (lambda ()
                            (local-set-key " " 'scroll-up)
                            (local-set-key "" 'scroll-down))))
      (add-hook 'sh-mode-hook
                (function (lambda ()
                            (local-set-key "<" 'self-insert-command))))
      (define-key isearch-mode-map "" 'isearch-repeat-forward)
      (define-key isearch-mode-map "" 'isearch-quote-char)))
;
(menu-bar-mode -1)
(defun terminal-keybindings ()
  "This function should be called by the term-setup-hook mechanism"
  (interactive)
  ; Set my own keybindings
  ;
  ; Set keybindings generally (including I-search on C-\)
;  (global-set-key "	" 'self-insert-command)
  (global-set-key "" 'isearch-forward)
;  (global-set-key "" 'newline)
  (global-set-key "" 'set-mark-command)
  (global-set-key "" 'quoted-insert)
  ;
  ; Set keybindings on esc-map
  (global-set-key "#" 'query-replace-regexp)
  (global-set-key "+" 'toggle-truncate-lines)
  (global-set-key "g" 'goto-line)
  (global-set-key "s" 'isearch-forward-regexp)
  (global-set-key "" 'eval-expression)
  ;
  ; Set keybindings on ^X-map.
  (global-set-key "m" 'vm-mail)
  (global-set-key "" 'save-buffer)
  (global-set-key "" 'toggle-read-only)
  (global-set-key "#" 'recover-file)
  (global-set-key "\12" 'bury-buffer)
  (global-set-key "" 'quicker-compile)
  (global-set-key "" 'exchange-point-and-mark)
  (global-set-key "" 'load-tramp)
  (global-set-key "g" 'magit-status)
  (global-set-key "g" 'magit-dispatch-popup)
  (global-set-key "4" 'scroll-other-window)
  (global-set-key "4m" 'vm-mail-other-window)
  (global-set-key "9" 'vm-visit-folder)
  (global-set-key "8" 'rmail-input)
  (global-set-key "p" 'cite-region)
  (global-set-key "F" 'mail-formletter)
  ;
  ; Set keybindings for Sun numeric pad.
  (setq esc-bracket-map (make-keymap))
  (define-key esc-bracket-map "D" 'backward-char)
  (define-key esc-bracket-map "C" 'forward-char)
  (define-key esc-bracket-map "A" 'previous-line)
  (define-key esc-bracket-map "B" 'next-line)
  (define-key esc-map "[" esc-bracket-map)
  (global-set-key "\e[214z" 'beginning-of-buffer)
  (global-set-key "\e[220z" 'end-of-buffer)
  (global-set-key "\e[216z" 'scroll-down)
  (global-set-key "\e[222z" 'scroll-up)
  (global-set-key "\e[192z" 'save-buffer)
  (global-set-key "\e[194z" 'save-some-buffers)
  (global-set-key "\e[195z" 'undo)
  (global-set-key "\e[193z" 'call-last-kbd-macro)
  (global-set-key "\e[196z" 'switch-to-buffer-other-window)
  (global-set-key "\e[197z" 'copy-region-as-kill)
  (global-set-key "\e[198z" 'find-file)
  (global-set-key "\e[199z" 'yank)
  (global-set-key "\e[198z" 'find-alternate-file)
  (global-set-key "\e[200z" 'find-file)
  (global-set-key "\e[201z" 'kill-region)
  (global-set-key "\e[-1z" 'info)
  ;
  ; Set keybindings for Chiarks' x11emacs
  (global-set-key "[5~" 'scroll-down)
  (global-set-key "[6~" 'scroll-up)
  ;
  ; Set keybindings for knackered vt100-like terminal emulators.
  (global-set-key "\eOt" 'scroll-down)
  (global-set-key "\eOu" 'scroll-up)
  (global-set-key "\eOD" 'backward-char)
  (global-set-key "\eOC" 'forward-char)
  (global-set-key "\eOA" 'previous-line)
  (global-set-key "\eOB" 'next-line))
(setq term-setup-hook 'terminal-keybindings)     ; remap keys in due course
;
(setq frame-title-format
      '(multiple-frames
	(:eval 
	 (let*
	     ((buf (current-buffer))
	      (leaf (buffer-name buf))
	      (dir default-directory))
	   (if dir
	       (let*
		   ((dir (abbreviate-file-name dir))
		    (node (replace-regexp-in-string "\\..*" "" system-name)))
		 (concat leaf "  " node ":" dir))
	     leaf)))
	("" invocation-name "@" system-name)))
;(setq frame-title-format
;      '(multiple-frames
;	(:eval 
;	 (let*
;	     ((buf (current-buffer))
;	      (fn (buffer-file-name buf)))
;	   (if fn
;	       (let*
;		   ((fn (abbreviate-file-name fn))
;		    (dir (file-name-directory fn))
;		    (leaf (file-name-nondirectory fn))
;		    (node (replace-regexp-in-string "\\..*" "" system-name)))
;		 (concat leaf "  " node ":" dir))
;	     (buffer-name buf))))
;	("" invocation-name "@" system-name)))
;
; Autoload definitions
(autoload 'tar-mode "tar-mode")
(autoload 'uncompress-while-visiting "uncompress")
; (autoload 'c++-mode "c++-mode" "Mode for editing C and C++ programs" t)
(autoload 'hide-ifdef-mode "hideif" "For editing code with #ifdefs" t)
;(setq-default indent-tabs-mode nil)
(autoload 'dired-find-alternate-file "dired-alternate" "Find alternately" t)
(autoload 'dired-run-file "dired-alternate" "Run this file" t)
(autoload 'perl-mode "perl-mode" "Mode for Perl code" t)
; (autoload 'rmail "rmail-fixed" "Patched mail reader" t)
(autoload 'quicker-compile "quicker-compile" "Patched Compile mode" t)

;(autoload 'vm "vm" "Start VM on your primary inbox." t)
;(autoload 'vm-visit-folder "vm" "Start VM on an arbitrary folder." t)
;(autoload 'vm-visit-virtual-folder "vm" "Visit a VM virtual folder." t)
;(autoload 'vm-mode "vm" "Run VM major mode on a buffer" t)
;(autoload 'vm-mail "vm" "Send a mail message using VM." t)
;(autoload 'vm-submit-bug-report "vm" "Send a bug report about VM." t)
;
;(require 'vm-autoload)

(vm-legacy-key-bindings)
;
; (autoload 'info "info-fixed" "Patched info browser" t)
(setq shell-mode-hook
      '(lambda ()
         (make-variable-buffer-local 'scroll-step)
         (setq scroll-step 1)))
(setq inferior-lisp-program "sbcl")
;(setq jit-lock-chunk-size 100
;      jit-lock-stealth-time 1.00)
(setq dired-mode-hook
      '(lambda ()
	 (define-key dired-mode-map "F" 'dired-find-alternate-file)
	 (define-key dired-mode-map "X" 'dired-run-file)))
;
; Prevent loading of default init file and do some of the things it did
; (setq inhibit-default-init t)
;
; Additional alist handling function
;(defun alist-remove-elem (s s-list)
;  "Delete the element of the alist S-LIST whose car is S"
;  (if s-list (if (equal s (car (car s-list)))
;		 (cdr s-list)
;	       (cons (car s-list) (alist-remove-elem s (cdr s-list))))
;    (nil)))
;
; Load C++ mode and edit Perl in perl mode
(setq auto-mode-alist
      (append '(("\\.[ch]$" . c-mode)
                ("\\.[CH]$" . c++-mode)
                ("\\.cc$" . c++-mode)
                ("\\.hh$" . c++-mode)
;                ("\\.tar$" . tar-mode)
                ("\\.pl$" . perl-mode)
                ("\\.ci$" . tex-mode)
                ("\\.asm$" . fundamental-mode)
                ("\\.Z$" . uncompress-while-visiting))
              auto-mode-alist))
(setq interpreter-mode-alist
      (append '(("sh" . fundamental-mode)
                ("bash" . fundamental-mode))
              interpreter-mode-alist))
;
; Use auto-fill when editing text
(add-hook 'text-mode-hook '(lambda () (auto-fill-mode 1)))
; (add-hook 'text-mode-hook '(lambda () (local-set-key "	" 'self-insert-command)))
;
; Set up newline to auto-indent & other stuff for perl, c++ and c modes.
(setq perl-mode-hook '(lambda () 
                        (local-set-key ";" 'self-insert-command)))
(setq c++-mode-hook '(lambda ()
                       (hide-ifdef-mode 1)
		       (local-set-key "," 'self-insert-command)
                       (local-set-key ":" 'electric-c++-terminator)))
; c++-electric-colon
(setq c-mode-hook '(lambda ()
                     (hide-ifdef-mode 1)
                     (c-set-offset 'substatement-open 0 nil)
		     (local-set-key "," 'self-insert-command)
		     (local-set-key ";" 'self-insert-command)
		     (local-set-key "/" 'self-insert-command)
		     (local-set-key "*" 'self-insert-command)
		     (local-set-key "#" 'self-insert-command)
		     (local-set-key "(" 'self-insert-command)
		     (local-set-key ")" 'self-insert-command)
		     (local-set-key "{" 'self-insert-command)
                     (local-set-key "" 'newline-and-indent)))
(setq c-hanging-comment-ender-p nil)
;
(defun vm-mail-other-window ()
  "Like `vm-mail' command, but display buffer in another window."
  (interactive)
  (switch-to-buffer-other-window (current-buffer))
  (vm-mail))
;
; mail precedences
(setq mail-precedence-key-alist
      '((?0  . "special-delivery")
        (?1  . "air-mail")
        (?2  . "first-class")
        (?3  . "second-class")
        (?5  . "third-class")
        (?\  . nil)
        (?6  . "bulk")
        (?9  . "junk")))
;
(defun mail-precedence-as-key ()
  "Set precedence by looking up last command char in mail-precedence-key-alist"
  (interactive)
  (message "%s" (concat "Precedence ["
                        (mapconcat '(lambda (c) (char-to-string (car c)))
                                   mail-precedence-key-alist "")
                        "] ?"))
  (let* ((key (read-char))
         (prec (assoc key mail-precedence-key-alist)))
    (if prec (mail-precedence (cdr prec))
      (error "mail-precedence-as-key `%s' not found" key))))
;
(defun mail-precedence-as-key-send-and-exit (arg)
  "Set precedence by looking up last command char in mail-precedence-key-alist,
then call send-and-exit."
  (interactive "P")
  (mail-precedence-as-key)
  (execute-kbd-macro ""))
;
(defun mail-precedence (prec)
  (save-excursion
    (mail-position-on-field "Precedence")
    (let ((p (point)))
      (beginning-of-line)
      (delete-region (point) p)
      (if prec
          (insert "Precedence: " prec)
        (delete-char 1)))))
;
(defun mail-mode-setup-keys ()
  (local-set-key "" 'mail-precedence-as-key)
  (local-set-key "p" 'mail-precedence-as-key-send-and-exit))
(add-hook 'mail-mode-hook 'mail-mode-setup-keys)
(add-hook 'vm-mail-mode-hook 'mail-mode-setup-keys)
;
(defun vm-mail-other-window ()
  "Like `vm-mail' command, but display buffer in another window."
  (interactive)
  (switch-to-buffer-other-window (current-buffer))
  (vm-mail))
;
(defun mail-formletter ()
  "Run VM-mail with ,Formletter"
  (interactive)
  (vm-mail)
  (rename-buffer (generate-new-buffer-name "form letter") t)
  (delete-region (point-min) (point-max))
  (insert-file (concat vm-folder-directory ",Formletter")))
;
; This function bound to C-x C-z
(defun load-tramp ()
  "Load tramp using require, if it isn't loaded already"
  (interactive)
  (message "Loading tramp...")
  (require 'tramp) ; -extended
  (message "Loading tramp...done"))
;
; This function bound to M-+
(defun toggle-truncate-lines ()
  "Toggle truncation or folding of long lines"
  (interactive)
  (set-variable 'truncate-lines (not truncate-lines))
  (redraw-display))
;
; RMAIL stuff
;(setq rmail-file-name "~/mail/RMAIL"
;      mail-archive-file-name "~/mail/Outbound"
;      rmail-primary-inbox-list '("~/mbox" "~/mail/Outbound" "~/mail/Record"
;                                 "~/mail/Import" "/var/spool/mail/ian")
;      rmail-delete-after-output t
;      rmail-last-rmail-file ""
;      rmail-ignored-headers (concat
;                             "^Content-Identifier:\\|^X400-[^O][A-Za-z-]+:\\|"
;                             rmail-ignored-headers))
;
(defun make-regexps-ignore-non-address (list)
  (apply 'append (mapcar
                  '(lambda (item)
                     (let ((item (mapconcat
                                  '(lambda (ch)
                                     (let ((s (char-to-string ch)))
                                       (if (string-match "[][.*+?^$\\]" s)
                                           (concat "\\" s)
                                         s)))
                                  item "")))
                       (list
                        (concat "<" item ">")
                        (concat "^" item " *\\((.*)\\)?$"))))
                  list)))
;
; BBDB
(setq bbdb-north-american-phone-numbers-p nil
      bbdb/mail-auto-create-p t
      bbdb-auto-revert-p t
      bbdb-notice-auto-save-file-p t
      bbdb-message-caching-enabled t)      
;
; VM stuff
(if (file-exists-p "~/private/private.el")
  (load-file "~/private/private.el"))
(if (file-exists-p "~/private/private2.el")
  (load-file "~/private/private2.el"))
(setq vm-included-text-attribution-format "%F writes (\"%s\"):\n"
      vm-reply-subject-prefix "Re: "
      vm-folder-directory "~/mail/"
      vm-delete-after-saving t
      vm-delete-empty-folders t
      vm-mutable-windows t
      vm-auto-get-new-mail nil
      vm-auto-next-message nil
      vm-jump-to-new-messages nil
      vm-jump-to-unread-messages nil
      vm-auto-displayed-mime-content-types '("text" "multipart")
      vm-mime-digest-discard-header-regexp "only-drop-this-header"
      vm-preview-lines nil
      vm-included-text-prefix "> "
      vm-confirm-quit 1
      vm-auto-center-summary t
      vm-confirm-new-folders t
      vm-circular-folders nil
      vm-visit-when-saving 0
      vm-move-after-deleting t
      vm-keep-sent-messages t
      vm-follow-summary-cursor t
      vm-frame-per-composition nil
      vm-frame-per-edit nil
      vm-frame-per-summary nil
      vm-frame-per-folder nil
      vm-tale-is-an-idiot t
      vm-primary-inbox (concat vm-folder-directory "INBOX")
      vm-sinbin-inbox (concat vm-folder-directory "SINBOX")
      vm-uninteresting-senders "ian"
      vm-reply-ignored-addresses
      (make-regexps-ignore-non-address
       '("ian@chiark.chu.cam.ac.uk" "ian" "iwj10@cus.cam.ac.uk"
         "ian@chiark.greenend.org.uk"
         "ijackson@nyx.cs.du.edu" "ijackson@gnu.ai.mit.edu"))
      vm-primary-inbox (concat vm-folder-directory "INBOX")

      vm-uninteresting-senders "ian"
      vm-reply-ignored-addresses
      (make-regexps-ignore-non-address
       '("ijackson@chiark.chu.cam.ac.uk" "ijackson"
         "iwj10@thor.cam.ac.uk" "iwj10@hermes.cam.ac.uk" "iwj10@cl.cam.ac.uk"
         "iwj10@cam.ac.uk" "Ian.Jackson@cl.cam.ac.uk"))
      mail-archive-file-name "~/mail/Outbound"


      vm-startup-with-summary t

      user-mail-address "ijackson@chiark.greenend.org.uk"


      vm-startup-with-summary nil
      vm-summary-format "%3n %a %2d %3m  %-19.19F  %s\n"
      mail-archive-file-name "~/mail/Outbound"
      vm-mime-8bit-text-transfer-encoding '8bit)
;
(add-hook 'vm-mode-hook
          '(lambda ()
             (local-set-key "Q" 'vm-quit)
             (local-set-key "q" "###Q")))
;(add-hook 'mail-mode-hook
;          '(lambda ()
;             (if (and (boundp 'folder-buffer)
;                      folder-buffer
;                      (string= (buffer-file-name folder-buffer)
;                               (file-truename vm-sysadmin-inbox)))
;                 (progn
;                   (make-local-variable 'vm-mail-header-from)
;                   (setq vm-mail-header-from
;                         "sysadmin@chiark.greenend.org.uk (Ian Jackson)")
;                   (make-local-variable 'mail-archive-file-name)
;                   (setq mail-archive-file-name
;                         "~/mail/SOutbound")))))
;
;;(defun vmi ()
;  (interactive)
;  (vm "/u/ijackson/mail/INBOX"))
;;
;(defun vms ()
;  (interactive)
;  (vm vm-sinbin-inbox))
;
(defun make-session ()
  "Makes this emacs hard to kill by requiring ^X^Cy to kill it
instead of just ^X^C."
  (interactive)
  (global-unset-key "")
  (global-set-key "y" 'save-buffers-kill-emacs))
;
; Fix problems with M-| setting window title of emacs' parent xterm
;(defun envdelete-term-termcap-windowid (list)
;  (let ((l list))
;    (mapcar '(lambda (x)
;               (if x (delq x l)))
;            (mapcar '(lambda (x)
;                       (if (or (string-match "^TERM=" x)
;                               (string-match "^TERMCAP=" x)
;                               (string-match "^WINDOWID=" x))
;                           x
;                         nil))
;                    l))
;    l))
;(setq process-environment
;      (envdelete-term-termcap-windowid process-environment))
;
(defun add-insertion-keys (table)
  "Adds keybindings according to TABLE.  Each element of
TABLE should be a four-element list.

BINDING should be the key to bind; FUNCTION will be the name of the
function defined to do the insertion; DESCRIPTION will be inserted
into \"Inserts ... .\", and used as the descriptive string for the
generated function; STRING is the string which will actually be
inserted into the buffer when the keystroke is pressed."
  (while table
    (let* ((head (car table))
           (key (car head))
           (func (nth 1 head))
           (desc (nth 2 head))
           (string (nth 3 head)))
      (defalias func
        (list
         'lambda () 
         (concat "Inserts " desc ".")
         '(interactive)
         (list 'insert string)))
      (funcall 'local-set-key key func))
    (setq table (cdr table))))

(defun ian-sgml-setup ()
  (interactive)
  (add-insertion-keys
   '(("" sgml-insert-new-para "new paragraph markup" "\n<p>\n")
     ("" sgml-insert-tt-emph "tt emph markup" "<tt/")
     ("" sgml-insert-prgn-emph "prgn emph markup" "<prgn/")
     ("" sgml-insert-em-emph "em emfh markup" "<em/")
     ("" sgml-insert-var-empfh "var emph markup" "<var/"))))
(setq sgml-local-catalogs '("/jura:/usr/lib/debiandoc-sgml/sgml/catalog"))
  
(add-hook 'sgml-mode-hook 'ian-sgml-setup)
;
(defvar cite-string "> " "String to insert when citing")
;
(defun cite-region (start end)
  "Quote each line in the region with an angle-bracket and space.

The citation string is taken from cite-string."
  (interactive "r")
  (save-excursion
    (save-restriction
      (narrow-to-region start end)
      (goto-char (point-min))
      (insert cite-string)
      (while
          (and
           (= (forward-line) 0)
           (> (point-max) (point)))
        (insert cite-string)))))
;
;

(winner-mode t)

(global-magit-file-mode)

(magit-wip-after-save-mode)
(magit-wip-after-apply-mode)
(magit-wip-before-change-mode)
(setq magit-no-confirm
      (append magit-no-confirm '(safe-with-wip)))

(setq vc-handled-backends (delq 'Git vc-handled-backends))

;; (defun hack-dir-local-variables ()
;;   "Read per-directory local variables for the current buffer.
;; Store the directory-local variables in `dir-local-variables-alist'
;; and `file-local-variables-alist', without applying them."
;;   (when (and enable-local-variables
;; 	     (buffer-file-name))
;;     ;; Find the variables file.
;;     (let ((variables-file (dir-locals-find-file (buffer-file-name)))
;; 	  (class nil)
;; 	  (dir-name nil))
;;       (cond
;;        ((stringp variables-file)
;; 	(setq dir-name (file-name-directory (buffer-file-name)))
;; 	(setq class (dir-locals-read-from-file variables-file)))
;;        ((consp variables-file)
;; 	(setq dir-name (nth 0 variables-file))
;; 	(setq class (nth 1 variables-file))))
;;       (when class
;; 	(let ((variables
;; 	       (dir-locals-collect-variables
;; 		(dir-locals-get-class-variables class) dir-name nil)))
;; 	  (when variables
;; 	    (dolist (elt variables)
;; 	      (unless (memq (car elt) '(eval mode))
;; 		(setq dir-local-variables-alist
;; 		      (assq-delete-all (car elt) dir-local-variables-alist)))
;; 	      (push elt dir-local-variables-alist))
;; 	    (hack-local-variables-filter variables dir-name)))))))

(delete ".git/" completion-ignored-extensions)

; End of this file.
